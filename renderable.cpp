#include "renderable.h"

Renderable::Renderable(QVector<Vertex> vertices,
                       GLenum renderMode) :
    m_rawVertices{vertices},
    m_vertexCount{static_cast<GLsizei>(vertices.size())},
    m_shader{nullptr},
    m_renderMode{renderMode}
{
    // Create Shader (Do not release until VAO is created)
    // TODO shaders need to be loaded somewhere else
    m_shader = new QOpenGLShaderProgram();
    m_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vertex/default.vsh");
    m_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fragment/default.fsh");
    m_shader->link();
    if(m_shader){ m_shader->bind(); }

    // Create Buffer (Do not release until VAO is created)
    m_vertices.create(); // TODO check for success
    m_vertices.bind();
    m_vertices.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_vertices.allocate(vertices.constData(), Vertex::stride() * m_vertexCount);

    // Create Vertex Array Object
    m_vbo.create();
    m_vbo.bind();
    if(m_shader){
        m_shader->enableAttributeArray(0);
        m_shader->enableAttributeArray(1);
        m_shader->setAttributeBuffer(0, GL_FLOAT, Vertex::positionOffset(), Vertex::PositionTupleSize, Vertex::stride());
        m_shader->setAttributeBuffer(1, GL_FLOAT, Vertex::colorOffset(), Vertex::ColorTupleSize, Vertex::stride());
    }
    // Release (unbind) all
    m_vbo.release();
    m_vertices.release();
    if(m_shader){ m_shader->release(); }
}

Renderable::Renderable(Renderable const& other) :
    Renderable{other.m_rawVertices, other.m_renderMode}
{}


Renderable::~Renderable()
{
    // Actually destroy our OpenGL information
    m_vbo.destroy();
    m_vertices.destroy();
    // TODO delete shader somewhere else
    if(m_shader){ delete m_shader; }
}

void
Renderable::render()
{
    // Render using our shader
    if(m_shader){ m_shader->bind(); }

    m_vbo.bind();
    glDrawArrays(m_renderMode, 0, m_vertexCount);
    m_vbo.release();

    if(m_shader){ m_shader->release(); }
}
