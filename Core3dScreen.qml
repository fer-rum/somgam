import QtQuick 2.0

import somgam 1.0

Item {

    width: 640
    height: 480

    Rectangle {
        color: "white"

        width: 100
        height: 50

        Text {
            id: helloText
            text: qsTrId("Hello World")
        }
    }

    RenderItem {}
}
