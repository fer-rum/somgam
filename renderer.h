#pragma once

#include <vector>

#include <QObject>
#include <QSize>
#include <QQuickWindow>
#include <QtGui/QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>

#include "renderable.h"

class Renderer :
        public QObject,
        protected QOpenGLFunctions
{
        Q_OBJECT
    private:
        QSize m_viewportSize;
        QQuickWindow* m_window;
        std::vector<Renderable> m_renderQueue;

    public:
        Renderer();
        ~Renderer();

        void setViewportSize(const QSize& size);

        /**
         * @brief setWindow
         * @param window the new Window to be set to.
         * Automatically adjusts viewport size.
         */
        void setWindow(QQuickWindow* window);

    public slots:
        void paint();
};
