#pragma once

#include <memory>

#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QVector3D>
#include <QOpenGLShaderProgram>

#include "vertex.h"

class Renderable
{
    private:
        QVector<Vertex> m_rawVertices;
        const GLsizei m_vertexCount;
        QOpenGLBuffer m_vertices;
        QOpenGLVertexArrayObject m_vbo;
        QOpenGLShaderProgram* m_shader;
        GLenum m_renderMode;

    public:
        Renderable(QVector<Vertex> vertices,
                   GLenum renderMode = GL_TRIANGLES);

        // allow copies
        Renderable(Renderable const&);
        ~Renderable();

        void render();

};
