QT += \
    qml \
    quick

SOURCES += \
    main.cpp \
    renderer.cpp \
    renderitem.cpp \
    renderable.cpp

HEADERS += \
    renderer.h \
    renderitem.h \
    vertex.h \
    renderable.h \
    primitives.h

RESOURCES += \
    user_interface.qrc \
    shaders.qrc

DISTFILES +=
