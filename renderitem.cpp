#include "renderitem.h"

RenderItem::RenderItem() :
    m_renderer(nullptr)
{
    qDebug() << "Creating RenderItem";
    connect(this, &QQuickItem::windowChanged,
            this, &RenderItem::handleWindowChanged);
}


void
RenderItem::handleWindowChanged(QQuickWindow* window)
{
    qDebug() << "Handle Window Changed";

    if(window == nullptr){
        qDebug() << "Window was null";
        return;
    }

    // reconnect and configure the new window
    connect(window, &QQuickWindow::beforeSynchronizing,
            this, &RenderItem::sync,
            Qt::DirectConnection);
    connect(window, &QQuickWindow::sceneGraphInvalidated,
            this, &RenderItem::cleanup,
            Qt::DirectConnection);
    // If we allow QML to do the clearing, they would clear what we paint
    // and nothing would show.
    window->setClearBeforeRendering(false);
}

void
RenderItem::cleanup()
{
    qDebug() << "RenderItem::cleanup";
    if(m_renderer){
        delete m_renderer;
        m_renderer = nullptr;
    }
}

void
RenderItem::sync()
{
    qDebug() << "RenderItem::sync";
    if (!m_renderer) {
        m_renderer = new Renderer();
    }
    m_renderer->setWindow(window());
}
