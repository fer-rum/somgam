#include <iostream>

#include <QtGlobal>
#include <QGuiApplication>
#include <QtQuick/QQuickView>

#include "renderitem.h"

int main(int argc, char* argv[]){

    // Things that always have to be done on startup:
    // * Parse command line parameters
    // * Load application configuration
    // * Preload required ressources

    QGuiApplication theApplication(argc, argv);

    // TODO register custom data types for QML
    qmlRegisterType<RenderItem>("somgam", 1, 0, "RenderItem");

    // Create the view and configure it
    QQuickView theView;
    theView.setResizeMode(QQuickView::SizeRootObjectToView);
    theView.setSource(QUrl("qrc:/screens/Core3dScreen.qml"));
    theView.show();

    return theApplication.exec();
}
