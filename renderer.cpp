#include "renderer.h"

#include "primitives.h" // TODO temporary

Renderer::Renderer() :
    m_renderQueue{}
{
    // just add some demo primitives to the render queue
    m_renderQueue.emplace_back(
                Triangle(
                    Vertex( QVector3D( 0.00f,  0.00f, 1.0f), QVector3D(1.0f, 0.0f, 0.0f) ),
                    Vertex( QVector3D( 0.50f, -0.00f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f) ),
                    Vertex( QVector3D(-0.00f, -0.50f, 1.0f), QVector3D(0.0f, 0.0f, 1.0f) )
                    )
                );
    m_renderQueue.emplace_back(
                Quad(
                    Vertex( QVector3D( 0.5f, -0.5f, 1.0f), QVector3D(1.0f, 1.0f, 1.0f)),
                    Vertex( QVector3D( 0.5f, -1.0f, 1.0f), QVector3D(0.0f, 1.0f, 1.0f)),
                    Vertex( QVector3D( 1.0f, -1.0f, 1.0f), QVector3D(1.0f, 0.0f, 1.0f)),
                    Vertex( QVector3D( 1.0f, -0.5f, 1.0f), QVector3D(1.0f, 1.0f, 0.0f))
                    )
                );
}

Renderer::~Renderer()
{}

void
Renderer::setViewportSize(const QSize& size)
{
    m_viewportSize = size;
    qDebug() << "Viewport set to " << size.width() << " x " << size.height();
}

void
Renderer::setWindow(QQuickWindow* window)
{
    Q_CHECK_PTR(window);

    m_window = window;
    setViewportSize(m_window->size() * m_window->devicePixelRatio());
    connect(m_window, &QQuickWindow::beforeRendering,
            this, &Renderer::paint,
            Qt::DirectConnection);
}

void
Renderer::paint()
{
    Q_CHECK_PTR(m_window);

    initializeOpenGLFunctions();
    // Make sure the shader programs are initialized

    glViewport(0, 0, m_viewportSize.width(), m_viewportSize.height());
    glDisable(GL_DEPTH_TEST);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    for(Renderable& current_renderable : m_renderQueue){
        current_renderable.render();
    }

    m_window->resetOpenGLState();
}
