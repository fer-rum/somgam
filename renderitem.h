#pragma once

#include <QQuickItem>
#include <QQuickWindow>

#include "renderer.h"

class RenderItem :
        public QQuickItem
{
        Q_OBJECT
    private:
        Renderer* m_renderer;
    public:
        RenderItem();

    public slots:
        /**
         * @brief sync
         * Updates the window to which the renderer is set.
         */
        void sync();
        void cleanup();

    private slots:
        /**
         * @brief handleWindowChanged
         * @param window The new window to which the renderItem belongs now.
         */
        void handleWindowChanged(QQuickWindow* window);
};
