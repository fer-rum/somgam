#pragma once

#include "renderable.h"

class Triangle : public Renderable
{
    private:
    public:
        Triangle(Vertex p0, Vertex p1, Vertex p2) :
            Renderable{QVector<Vertex>{p0, p1, p2}}
        {}
};

class Quad : public Renderable
{
    private:
    public:
        /**
         * @brief Quad is a shape with four corner points.
         *  The points are expected to be  given on clockwise order
         *  when viewed from the front.
         * @param p0
         * @param p1
         * @param p2
         * @param p3
         */
        Quad(Vertex p0, Vertex p1, Vertex p2, Vertex p3) :
            Renderable{
                QVector<Vertex>{ p0, p1, p3, p2 }, // Reorder for triangle strip
                GL_TRIANGLE_STRIP // Note that GL_QUADS seems to be outdated
             }
        {}
};
